%
% $Id: smc05.tex,v 0.11 2006-05-23 21:23:59 nicb Exp $
%
% -----------------------------------------------
% Template for SMC05
%    smc.sty -> style file
% By Eloi Batlle (eloi@iua.upf.es), changes for 
% SMC05 by Domenico Vicinanza (dvicinanza@unisa.it)
% -----------------------------------------------

\documentclass{article}
\usepackage{smc05,amsmath}
\usepackage{graphicx}
\usepackage{color}
\usepackage{rcs}
\RCS $Revision: 0.11 $
%\RCS $Date: 2006-05-23 21:23:59 $
\usepackage[final]{prelim2e}
\renewcommand{\PrelimWords}
{%
	Preliminary Version \RCSRevision\ %
}
\usepackage{version}
\excludeversion{comment}
\newcommand{\Comment}[1]{\begin{comment}\textcolor{blue}{\ Comment: #1}\end{comment}}
\usepackage{paralist}
\usepackage{mdwlist}
\usepackage{hyperref}
\newcommand{\hhref}[1]{\href{#1}{#1}}
\usepackage[english]{babel}
\usepackage{varioref}
\usepackage{changebar}
\nochangebars % set when final
\newcommand{\imagedir}{../../images}
\newcommand{\squeezelist}
{%
	\addtolength{\topsep}{-40pt}
	\addtolength{\itemsep}{-1pt}
	\addtolength{\parsep}{-40pt}
}
\newcommand{\copyrightnotice}[3]
{%
	\includegraphics[width=1.45cm]{\imagedir/cc}
	\parbox{8cm}
	{\tiny%
		Copyright $\copyright$ {#3}\ {#1}\\
		This work comes under the terms of the Creative Commons $\copyright$ BY-SA 2.5 license\\
		(\href{http://creativecommons.org/licenses/by-sa/2.5/}{http://creativecommons.org/licenses/by-sa/2.5/})
	}
}


% Title.
% ------
\title{Sustainable Live Electro-Acoustic Music\footnotemark}

% Single address
% To use with only one author or several with the same address
% ---------------
%\oneauthor
%  {Author} {School \\ Department}

% Two addresses
% --------------
\twoauthors
  {Nicola Bernardini} {Conservatorio di Padova \\ Padova, Italy}
  {Alvise Vidolin} {Conservatorio di Venezia \\ Venezia, Italy}

% Three addresses
% --------------
%\threeauthors
%  {First author with an exceptionally long name} {School \\ Department}
%  {Second author} {Company \\ Address}
%  {Third author} {Company \\ Address}
\newcommand{\ea}{electro--a\-cous\-tic}
\newcommand{\lea}{live \ea}
\newcommand{\Lea}{Live \ea}

\begin{document}
\sloppy
%
\maketitle
%
\footnotetext
{%
	\copyrightnotice{2005}{nicb@sme-ccppd.org, vidolin@dei.unipd.it}%
	{Nicola Bernardini, Alvise Vidolin}%
}

\begin{abstract}
Real--time/performed electro--acoustic music (also known as \emph{\lea}\ music)
is currently facing a serious sustainability problem:
while its production is indeed considered very recent from the music history
point of view, several technological generations and revolutions have gone by in the
meantime. Thus, most of these works can hardly be performed because the
technologies used have gone lost since a long time and no long-standing
notational precaution was taken.
This paper presents some typical case studies and examples
and introduces some techniques that might
lead to a partial -- when not completely adequate --
solution to the sustainability problem.
\end{abstract}
%
\section{Introduction\label{sec:introduction}}
%
Digital preservation and archival of cultural assets is now a widely-studied and
active research problem everywhere
(cf.\cite{keeper:marcum2003,preser:porck2000,avoidt:rothenberg1999,preser:lesk1995}).
The music domain is no exception to this rule,
ranging from the preservation of score ma\-nu\-scrip\-ts
to that of antique musical instruments,
old recordings\footnote
{%
	cf.\hhref{http://www.aes.org/technical/documentIndex.cfm\#ardl}
}, electro-acoustic music on tape (cf.\cite{Canazza01,Canazza02}), etc.
In general, from these studies it appears that digital preservation
of \emph{dense} documents\footnote
{%
	the term \emph{dense} is taken from early semiotic studies
	(cf.\cite[p.241 and sec.3.4.7]{Eco75}, \cite[III, 3]{Goodman68} and
	\cite{Ber89}).
	It basically means documents that carry all the information within
	themselves, i.e. they are not symbolic representations to be further
	interpreted and converted into a final artifact.%
}%
coupled with symbolic representation of linguistic elements (where available)
would be sufficient to preserve most artistic works in the music domain.

There is a specific music field, however, which presents many more problems
in the preservation of its works: \lea\ music. Most, if not all, \lea\ works 
are endangered today because their sustainability in time is extremely low
(cf. Sec.\vref{sec:problems}) -- there is an urgent need for research and solutions
to face an otherwise inevitable loss of many masterpieces of the past century.
Furthermore, if the sustainability problems of \lea\ music are not tackled,
current and future works may well face the same fate of their predecessors.

\section{Problems}\label{sec:problems}

\Lea\ music is indeed a ``performance--intensive'' art form which may
somehow relate to other similar musical formats: jazz, popular music or
``performer--centered'' interpretations are the first that come to mind.
These may include, for example,
the jazz standards performed by an extraordinary artist, rock-band concerts
and records, sublime interpretation of classical works by legendary singers
or players, etc.
In general, preserving these formats implies preserving the
recorded documents that contain them. While that is not the performance
\emph{per se}, its high-quality reproduction is deemed acceptable
for memory preservation.

\Lea\ music is different in that we seek to preserve not only a single,
memorable performance but rather the ability to 
to perform, study and re-interpret the same work over
and over again, with different performances proposing different
interpretations.
A recorded document of the first (or indeed, of any) performance of a \lea\
music work is instead completely insufficient and inadequate to the
re-creation of the work itself.

Of course, this would call for a score capable of providing
the necessary performance indications to the complete re-construction
of the piece. Symbolic notation, abstracted from practical implementation
and the underlying technology, is extremely important here.
In this case, notation should be both descriptive and prescriptive to some
extent (it should define \emph{which} result is sought and \emph{how} to get
it --- always in device--independent terms).
However, \lea\ music currently possesses notational conventions and practices that can be
compared at best to tablatures of the Middle Ages. This is due to several
factors, the most important being the availability of recording technology
which has been considered, for years, as the proper way to preserve the
details concerning the \ea\ performance. This, in connection with
\begin{enumerate}[a)]
\squeezelist

	\item use of end--user configuration patches using proprietary
	      software and hardware technologies (cf. Sec.\vref{sec:okto});

	\item use of binary and especially proprietary file formats
	
\end{enumerate}
has lead to huge losses in performance information of many \lea\ music
works. It is now time to think about the sustainability of these 
past, present and future works.

\section{Possible Solutions}\label{sec:solutions}

Scores are essential to \emph{speculative} (i.e.: non--commercial, non--programme)
music to preserve two fundamental
musical functions, namely reproduction and interpretation of the works;
most \lea\ music is no exception to this requirement.
Full-blown audio-video recordings are not appropriate, because
\begin{enumerate}[a)]
\squeezelist

	\item recordings do not convey any of the necessary instructions
	      (descriptive and prescriptive) that are required to re-interpret the work,
	      and

	\item they give a ``reference'' interpretation to mimic, thus
		  seriously jeopardizing the possibility of new interpretation.

\end{enumerate}

Furthermore,
the score representation must resist time degradation and technological revolutions, so
it must rely on lower level standard common denominators (such as
paper, widely diffused sound file formats, standard metric units, etc.).

These considerations have lead to the following solutions that should be adopted to
for any sustainability-aware live electro-acoustic score --- it should be augmented with

\begin{enumerate}
\squeezelist
	\item a multimedia glossary covering all \lea\ 
	      processing found in the work; every item should have:
		 \begin{itemize}
		 \squeezelist

		 	\item an algorithmic description
			
			\item an impulse response 

			\item an audio example

		\end{itemize}
		these items should be provided in an ASCII-based
		standardized format (such as XML).

	\item a computer--assisted notation system based on the orchestra/score
	      paradigm (i.e. a des\-crip\-tion/pres\-crip\-tion of \emph{how} sounds are
		  built, and a des\-crip\-tion/pres\-crip\-tion of \emph{when} they should be
		  built in ti\-me).

\end{enumerate}

We maintain that a multimedia glossary provides indeed completely different 
information than a full-blown recording of a passage or an entire piece. A
glossary such as the one devised above
allows performers to verify if single elements are in place while leaving
most of the interpretation (e.g. quantities, speeds, etc.) to them.
Furthermore, while the impulse response of a processing system possesses the
abstraction qualities that we seek, it would probably be too hard, in most
cases, to trace back the (usually complex) system producing it. The impulse
response should then be complemented by an accurate algorithmic description
of each electro-acoustic process and the impulse response (or its transfer
function) should rather serve as a checking tool (much like the test--tones
were used on electro-acoustic tapes in the analog era).

The orchestra/score paradigm is still relevant because it allows
\begin{enumerate}[a)]
\squeezelist

	\item good separation of \emph{system} data from
	      \emph{performance} data;

	\item lowering of the complexity barrier during performance;

	\item seamless integration with traditional instrument performance.

\end{enumerate}

True, the orchestra/score has a number of serious drawbacks
(cf. for example \cite{Lazzarini98}), the most important being
that of promoting a mind-set which separates processes from events.
This separation is often very weak, if not utterly non--existent,
in \ea\ music as well as in much contemporary music thinking.
However, the lack of a distinctly better model and the advantages
enumerated above suggest that this paradigm can still find its use
in \lea\ music.

The adoption of sustainability--aware score models may be considerably
facilitated by
computer--assisted notation software applications that should be devised to:
\begin{enumerate}[a)]
\squeezelist

	\item pick up automatically internal data of electronic devices such
	      as mixers, effects, DSPs, etc. \emph{and} convert it to a common
		  format (such as XML) using ASCII, standard metric units, etc.;

	\item provide objective representations such as time--local impulse response
	      plots;

	\item provide assistance to the composer in devising the performance
	      notation related to the above elements.

\end{enumerate}

Along the same lines, \lea\ music transcription and documentation
should constitute an integral part of the creation of a work (just as writing a score is
essential to most contemporary music) and it could/should become an editorial
profession just as professional music copying is.

These elements should provide long--standing sustainability to
\lea\ music works.

\section{Case Studies\label{sec:cs}}

\begin{changebar}
\Lea\ music is quite an abundant field which provides many examples with much
diversity of contexts and settings. Unfortunately, most of these examples
are problematic: the way the scores are realized do not allow the performance of
the score any longer or will not allow it in the very short future (we must
always think that in music 50 years is a \emph{very short} future). Of course,
a ``problematic'' example of \lea\ music scoring has nothing to do with the
quality of the music itself. However, a ``problematic'' example of scoring of a
great piece will not allow its performance any longer and that makes it even
more problematic --- if at all possible.

Luckily, a few ``positive'' examples exist. No example is really perfect and
many problems still need to be solved, but these positive examples
are very important because they create a foundation that can be taken
over and enhanced.

\newcommand{\okto}{\emph{Oktophonie}}
\subsection{Stockhausen -- \okto}\label{sec:okto}

A good example is provided by \okto\ 
by Karlheinz Stockhausen \cite{stockhausen:oktophonie}.
\okto\  is a 69--minute multi-channel tape piece which,
in theory, could also do without a score.
\begin{figure}[htb]
\centerline{\framebox{
	\includegraphics[width=\columnwidth]{\imagedir/Oktophonie-O_V}}}
\caption{Stockhausen, \okto\ --- Some pictures of Page O V}
\label{fig:okto_O_V}
\end{figure}
However, faithful to his long-standing tradition
of creating beautiful realization scores,
Stockhausen has carefully notated every musical and technical detail of \okto\ 
to a excruciating definition level. The score has also a long introduction
(32 pages almost entirely repeated twice, in German and in English)
in which the process of creating and reproducing the music is described at
length.
Thus, we may think that this is a good example of an electronic music
work sufficiently described to reconstruct the piece forever and ever.
However, a deeper look (and even more, an attempt at reconstructing the piece)
will unveil a few dark spots like the passing references
to technology and software such as the now-legendary
``Atari 1040ST'', the ``QUEG (Quadraphonic Effect Generator)''
and the ``Notator version 2.2'' sequencer software.
These references are
completed only by photographic evidence -- cf. Fig.\vref{fig:okto_O_V},
which unfortunately will not say much about the inner workings
of these devices.
\begin{figure}[htb]
\centerline{\framebox{
	\includegraphics[width=\columnwidth]{\imagedir/Oktophonie-O_IV}}}
\caption{Stockhausen, \okto\ --- Page O IV, Details of the Schematic Description}
\label{fig:okto_O_IV}
\end{figure}
The rest of the technical introduction contains a schematic
description of the production system
(cf. Fig.\vref{fig:okto_O_IV} -- please note the reference to the ``Notator
diskettes'', with no further information of their contents), and the timings
and dynamics of every track in every section.
We are at a loss concerning the description of how the ``QUEG'' used to handle
sound spatialization (inter--channel interpolation, measured amplitude ranges,
etc.). The only reference on the web\footnote
{%
	\hhref{http://www.ems-synthi.demon.co.uk/emsprods.html\#queg}
}%
does not help much either.
The ``Atari 1040ST'' has become a true museum piece\footnote%
{%
	cf. for ex. \hhref{http://www.atarimuseum.com/computers/16bits/stmenu/atarist.htm}%
}%
and the company itself has long since gone into more profitable businesses.
Emagic Gmbh stopped supporting the Atari platform at the beginning of
the new century and has been bought by Apple Inc. in 2002.
The company has refused to release the source code or the binaries of the
``Notator'' program claiming that
``it could steal potential Notator Logic customers''\footnote%
{%
	\hhref{http://www.notator.org/html/notator\_faq.html\#17}%
	(``Notator Logic'' being a completely different software running on
	Macintosh platforms)
}%
-- so any form of
data based on the ``Notator Sequencer'' running on an Atari platform is basically 
lost. Concerning this last point, there is only one chance: there is a
a (possibly still on--going) voluntary community of affectionate ``Notator'' users\footnote%
{%
	\hhref{http://www.notator.org}%
}%
which may help out with the diskettes
(this is important because it shows a clear case on a central issue in memory
conservation --  the power of communities versus
the unreliability of companies).

In a case like this, we can
only be very happy that a tape exists, because until that tape exists
\okto\  will exist. A faithful reconstruction is really difficult,
if at all possible, because some essential information is missing.

This work is indeed the most significant we could find under several aspects.
\begin{enumerate}
\squeezelist
	\item
\okto\  provides a sufficient time perspective
to show the main problem of \lea\ music performance. 
Judging by music history time scales, \okto\  is
a \emph{very very} recent work (it is dated 1990/1991) --- musicologists
would indeed consider it totally \emph{contemporary}.
	\item
it shows quite clearly that time scales of
technology and software are very different: the technology described
in the score has been obsolete \emph{by several generations} now. It is hardly
available or in working condition anywhere on the planet. If the realization
of the score relies on the presence of this technology, then the work is
irremediably lost unless a re--edition of the realization score is worked out,
itself obsoleting the first edition.
\end{enumerate}
Thus, while we acknowledge that
Stockhausen and his collaborators have worked very hard on
the score of \okto\ to provide all the information required
to reconstruct the piece, the score itself
is the perfect example of how hard the problem of sustainability of
\ea\ music is. The main point being:
a reference to the technology used is simply not enough to
reconstruct the piece.

However, \okto\ is certainly not the most endangered work.
The current trend
of many \lea\ music leads to many disconcerting examples.
%
Seeking
precision and detail, composers produce scores which do
include ``the live-electronic part'' saved digitally (often using proprietary
undisclosed format) on some media fitted to the
purpose, using some (often proprietary) software application fitted to the
purpose which runs on casual operating systems and hardware. \Lea\ performers
are being told ``You just press ''Play'' and everything starts'', and that
seems to be the ultimate solution. It is in fact the ultimate grave for
these works. Just consider that many of these works save the data/application
on Iomega Zip Drives (now being discontinued) or non-industrial masterings
on CD-ROM. As far as media go, the latter ones may last much longer through
several backward--compatible editions, but will they last 50 or 100 years?
will they last longer than that? We still consider Arnold Sch\"onberg's
\emph{Pierrot Lunaire} ``recent'', don't we?

\newcommand{\cenci}{\emph{The Cenci}}
\subsection{Battistelli, \cenci}\label{sec:cenci}

Other examples may be less problematic.
Here, ``less problematic'' does not mean that the
authors are absolutely sure that these scores are indefinitely performable. All
the scores listed below still carry sustainability problems which we will try
to point out. However, these scores show some successful attempts at
sustainability. ``Successful'' means that attempts to perform the score
without non-score hints from the composers and or her/his technical assistants
do actually succeed\footnote
{%
	Often the problems are experienced by the composer herself/himself and/or
	her/his technical assistants when they try to pick up the work for another
	performance years after the premi\`ere.
}.
\end{changebar}
\newcommand{\cenci}{\emph{The Cenci}}

Giorgio Battistelli's \cenci\cite{battistelli:cenci} (1997) provides another
good initial example.
The score possesses a detailed legend both for the symbolic
notation used for the actors' voices and for that used for the \lea processing
of orchestra and voices.
\begin{figure}[htb]
\centerline{\framebox{
	\includegraphics[width=\columnwidth]{\imagedir/cenci_06}}}
\caption{Battistelli, \cenci\ --- Processing definition (example)}
\label{fig:cenci_orc}
\end{figure}
As an example, fig.\vref{fig:cenci_orc} describes processing n.6. The graphical
description shows:
\begin{itemize}
 \squeezelist

 	\item the input/output flow
			
	\item the algorithm (in abstract terms)

	\item the properties of the object (i.e. the values) expressed
	      in conventional units (i.e. Hertz, dB, etc.)

\end{itemize}
In the score, the processing is called on and off by a very simple and visible
graphic device (shown in fig.\vref{fig:cenci_sco}).
\begin{figure}[htb]
\centerline{\framebox{
	\includegraphics[width=\columnwidth]{\imagedir/cenci_score-91}}}
\caption{Battistelli, \cenci\ --- Processing calling (example)}
\label{fig:cenci_sco}
\end{figure}
Score and \lea\ instructions are completely provided in
technology--independent terms within the sheet music.
The score explains in detail \emph{how} to create every single processing device used
within the work\footnote
{%
	there are 11 descriptions similar to that shown in fig.\vref{fig:cenci_orc}
	and a glossary of 27 vocal effects in the score.
}, and then it shows precisely \emph{when} it is to be performing in the
music.
As such, Battistelli's \cenci\ is fairly sustainable.
Of course, the addition of impulse responses and isolated audio examples
for each processing would indeed complete the picture, but
\begin{changebar}
the composition may already be re-constructed by the score alone
as in fact it has been done at least once after the premi\`ere
performances with the original technical staff\footnote{%
	notably at the Hebbel--Theater in Berlin in 1999, under the
	sound direction of sound direction of Mark Polscher.
}. To be completely precise, a member of the original production team
(Alvise Vidolin) was asked by the composer to join the sound crew
towards the end of this latter production to help him (the composer) out with
some final loose ends, but that had hardly anything to do with the
way \cenci\ was scored.
\end{changebar}

Later scores by Giorgio Battistelli are developed along the same lines, with a
varying degree of detail and definition (cf. for example
\cite{battistelli:embalmer}).

\newcommand{\ddd}{\emph{Dialogue de l'Ombre Double}}

\subsection{Boulez, \ddd}\label{sec:dialogue}

Pierre Boulez's \ddd\cite{boulez:dialogue} (1984) for solo clarinet and
live--electronics provides another brilliant earlier example. The \ea\ setting
implies in this case some special miking of the clarinet, a natural
reverberator built out of the resonances of a piano and the performance
of a (recorded) ``shadow'' clarinet in a spatialized context in transitional
passages between (real) clarinet solos.

\begin{figure}[htb]
\centerline{\framebox{
	\includegraphics[width=\columnwidth]{\imagedir/Dialogue-sigle_final}}}
\caption{Boulez, \ddd\ --- Transition description (example)}
\label{fig:ddd}
\end{figure}
Each passage is described in plain words in a separate part of the score (an
example is shown in fig.\vref{fig:ddd}). Levels and volumes of each elements
are expressed in proportional form in tenths (i.e. $1/10$, $2/10$, \ldots),
\begin{figure}[htb]
\centerline{\framebox{
	\includegraphics[width=\columnwidth]{\imagedir/Dialogue-spatialisation}}}
\caption{Boulez, \ddd\ --- Sound location description (example)}
\label{fig:dddspat}
\end{figure}
timings are in seconds, and sound location is expressed in terms of 
speakers going on or off at given cues in the score (again, in
orchestra/score functional distribution --- cf. fig.\vref{fig:dddspat}).
\begin{changebar}
No references to specific technologies are made.
\end{changebar}

\ddd\ is a difficult virtuoso piece both for the clarinet part and for the
live--electronics part. However, it can be easily picked up, studied and
re--in\-ter\-pre\-ted from the score alone\footnote
{%
	here too, there are many performances by several different players and
	teams.
}. There are indeed a few problems in the correct interpretation of dynamic
balances (the scales are neither scientific --- such as $0dB$, $-12dB$, etc.
--- nor musical --- such as {\it mf}, {\it fff}, etc.), but the overall scheme is very
well thought out and it enforces sustainability.

\newcommand{\dak}{\emph{das Atmende Klarsein}}
\subsection{Nono, \dak}\label{sec:dak}

Luigi Nono's scores of his late \ea\ works have always been
seriously endangered: the early scores derived from his manuscript
were really too scarce in providing information to reconstruct a work from
that repertoire. These works could only be performed by an extremely small
group of gifted musicians which were personally trained and instructed by Nono
for each work, and the electronics were no exception to this.

However, musicians and technicians along with the \emph{Archivio Luigi Nono}
have collected abundant notes and documentation over
the years to reconstruct the late works in every details, and when publisher
BMG--Ricordi decided to provide a new edition to each of these works,
they were ready to answer the call.

Nono's \dak\cite{nono:dak} (1987) is one of the first examples
of this daunting endeavor, and it
is indeed one of the most optimal examples so far.

\begin{figure}[htb]
\centerline{\framebox{
	\includegraphics[width=\columnwidth]{\imagedir/Dak_08}}}
\caption{Nono, \dak\ --- Processing description (example)}
\label{fig:dakdesc}
\end{figure}
\begin{figure}[htb]
\centerline{\framebox{
	\includegraphics[width=\columnwidth]{\imagedir/Dak_09}}}
\caption{Nono, \dak\ --- Score excerpt}
\label{fig:dakscore}
\end{figure}
Here too,
the score is provided with a detailed description of each processing
(cf. fig.\vref{fig:dakdesc})
along with a graphic pattern for performance (cf. fig.\vref{fig:dakscore}).
In addition, the 2005 edition of score comes with a DVD
which contains:
\begin{itemize}
\squeezelist

	\item a historical introduction to the genesis of the work

 	\item a commented performance (both for the flute
	      and the live--electronics parts)
			
	\item an audio glossary of flute effects

	\item an introduction to the performance of the live--electronics part

	\item an overview of the performance practices of the choir

\end{itemize}
Most comments and introductions are provided by the performers who
actually worked with Nono on the premi\`ere of the work.

It should be noted that the DVD does \emph{not} carry a simple recording of
the complete performance. As stated above (cf. Sec.\vref{sec:problems}),
a simple recording would probably jeopardize the possibility of other
interpretations.

A useful addition could be, here too, the presence of impulse responses
for each processing element, and the replacement of references to specific hardware
(i.e. the \emph{Halaphon} --- which is nonetheless a well--documented instrument though)
with the abstract functionalities of that hardware.

\subsection{Harder Problems: spatialization}\label{sec:spatialization}

Representation of sound location in space still remains a harder problem
to solve. Solutions such as the one adopted in Boulez's \ddd\ (cf.
fig.\vref{fig:dddspat}) work for relatively simple movements and settings.
When things get more complicated, the space--time characteristic of sound
location still poses great challenges to concise symbolic notation
that can be learned by performers out of the score alone.

\begin{changebar}
\section{Conclusions}\label{sec:conclusions}

We hope to have raised with this paper the attention
over a problem whose solution is ever more urgent:
that of the sustainability of \lea\ music works.

Since the first draft of this paper we have noticed at least one other
paper devoted precisely to this issue (cf.\cite{polfreman2005}) in a
knowledgeable and documented way. The authors provide interesting case studies
of work reconstructions of two complex works by Luigi Nono
(\emph{Quando Stanno Morendo, Diario Polacco No.2} and
\emph{Omaggio a Gy\"orgy Kurtag}). However, these studies concentrate
on the technology needed \emph{today} to reconstruct the pieces,
and they fail to consider that the real ``infinite'' reproduction
can only be obtained
by creating adequate notation and transcription methods.
The authors rely on specific current technology and hardware, thus simply
postponing the problem to a later stage, maybe ten, twenty or thirty years
from now.

Instead, we strongly maintain that \lea\ works
will stand a better chance of sustainability
if their score will rely upon:
\begin{itemize}
\squeezelist
	\item \emph{low} technology or no technology at all (paper, ink,
	      standard measure units, etc.)
	\item redundancy of sources (wider diffusion, perhaps obtained via
	      \emph{P2P} technologies and open licensing schemes)
	\item isolated audio and impulse response examples recorded following
	      standardized codings on sufficiently diffused media
		  (such as the CD or the DVD media, though we acknowledge the problems
		  which these media may encounter fifty years from now)
	\item last but not least, active communities of
	      co--operating performers which will be conscious enough
		  to share and document their experiences
	      (implying of course the on--going performances of the works themselves)
\end{itemize}

In particular, any dependency on any form of computing platform and software
should be strongly avoided in the scores.
\end{changebar}

\bibliographystyle{plain}
\bibliography{../../EMN}

\end{document}
